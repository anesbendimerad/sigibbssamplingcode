package model;

import java.util.HashMap;

public class Database {
	public int[][] transactions;
	public HashMap<Integer2,Integer>[] transactionsMaps;
	public String[] columnsNames;
	public String[] linesNames;
	public double density;
	public int maxSizeOfTrans=-1;
	public Database(int[][] transactions, String[] columnsNames, String[] linesNames,int maxSizeOfTrans) {
		this.transactions = transactions;
		this.columnsNames = columnsNames;
		this.linesNames = linesNames;
		this.maxSizeOfTrans=maxSizeOfTrans;
		computeDensity();
	}
	
	
	public void computeDensity() {
		density=0;
		for (int[] trans : transactions) {
			density+=trans.length;
		}
		density/=((double)(transactions.length*columnsNames.length));
	}
	
	public void updateTransactionsMaps(){
		transactionsMaps=new HashMap[transactions.length];
		int cpt=0;
		for (int[] transaction : transactions){
			transactionsMaps[cpt]=new HashMap<>();
			for (int i=0;i<transaction.length;i++){
				transactionsMaps[cpt].put(new Integer2(transaction[i]),i);
			}
			cpt++;
		}
	}

}
