package model;

import java.util.LinkedList;

public class Pattern {
	public int id;
	public LinkedList<Integer> columns;
	public LinkedList<Integer> rows;
	public double ic;
	public double si;
	public double[] itemsetIC;
	public double[] rowsIC;
	public int support;
	public int nbGeneratedTimes;
	
	private int hashcodeValue;
	private boolean hashcodeComputed=false;
	public static int nbTotalColumnsInD=-1;
	
	public Pattern(LinkedList<Integer> itemset,LinkedList<Integer> rows, double ic, double si) {
		this.columns = itemset;
		this.ic = ic;
		this.si = si;
		this.rows=rows;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pattern)){
			return false;
		}
		Pattern other=(Pattern)obj;
		if (columns.size()!=other.columns.size()){
			return false;
		}
		if (rows.size()!=other.rows.size()){
			return false;
		}
		for (int i : columns){
			boolean found=false;
			for (int j : other.columns){
				if (i==j){
					found=true;
					break;
				}
			}
			if (!found){
				return false;
			}
		}
		for (int i : rows){
			boolean found=false;
			for (int j : other.rows){
				if (i==j){
					found=true;
					break;
				}
			}
			if (!found){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		if (!hashcodeComputed) {
			hashcodeValue=0;
			for (int i : columns) {
				hashcodeValue+=hash(i);
			}
			for (int j : rows) {
				hashcodeValue+=hash(j+nbTotalColumnsInD);
			}
		}
		hashcodeComputed=true;
		return hashcodeValue;
	}
	
	private int hash(int x) {
		x = ((x >> 16) ^ x) * 0x45d9f3b;
		x = ((x >> 16) ^ x) * 0x45d9f3b;
		x = (x >> 16) ^ x;
		return x;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		Pattern p=new Pattern(new LinkedList<>(columns), new LinkedList<>(rows), ic, si);
		p.support=support;
		p.nbGeneratedTimes=p.nbGeneratedTimes;
		return p;
	}
}
