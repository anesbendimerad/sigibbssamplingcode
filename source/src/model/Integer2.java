package model;

public class Integer2 {
	public int value;

	
	public Integer2(int value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		return (value == ((Integer2) obj).value);
	}

	@Override
	public int hashCode() {
		int x = value;
		x = ((x >> 16) ^ x) * 0x45d9f3b;
		x = ((x >> 16) ^ x) * 0x45d9f3b;
		x = (x >> 16) ^ x;
		return x;
	}

}
