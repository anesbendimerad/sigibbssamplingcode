package model;

import utils.Utilities;

public class SubjectiveData {
	public Database database;
	public double[] lambdaLines;
	public double[] lambdaColumns;
	public double[][] iCPerCell; // same indices as transactions in database
	public double[] iCPerLine; // same indices as transactions in database
	public double alpha; // log(1-p/p), p is density
	public double gamma; // log(1/1-p), p is density
	public SubjectiveData(Database database, double[] lambdaLines, double[] lambdaColumns, double[][] iCPerCell,
			double[] iCPerLine) {
		this.database = database;
		this.lambdaLines = lambdaLines;
		this.lambdaColumns = lambdaColumns;
		this.iCPerCell = iCPerCell;
		this.iCPerLine = iCPerLine;
		if (database.density==0.5) {
			throw new RuntimeException("density is 0.5, this specific case needs some changes in the code, otherwise results will be wrong");
		}
		alpha=Utilities.log2((1.-database.density)/database.density);
		gamma=Utilities.log2(1./(1.-database.density));
	}
	
	
	
	
	 
}
