package controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.lucene.util.OpenBitSet;

import model.Measure;
import model.Pattern;
import model.SubjectiveData;
import utils.DesignPoint;
import utils.Statistics;
import utils.Utilities;

public class PatternSampler {
	public DesignPoint designPoint;
	public Statistics statistics;
	public Pattern[] sampledPatterns;
	public ArrayList<Integer> topKPatternsList;
	public Pattern[] nonRedundantPatterns = null;
	public SubjectiveData subjData;
	private OpenBitSet intermBitSet;
	private BitsetSupportComputer supportComp;
	private int nbGibbsIterations = 100;
	public HashMap<Pattern, Pattern> nonRedundantMap;
	public double[] twoPowersTab;

	protected double[] transacWeights;

	public PatternSampler(DesignPoint designPoint, Statistics statistics, SubjectiveData subjData,
			BitsetSupportComputer supportComp) {
		this.designPoint = designPoint;
		this.statistics = statistics;
		this.subjData = subjData;
		intermBitSet = new OpenBitSet(subjData.database.transactions.length);
		this.supportComp = supportComp;
		supportComp.initBitSets();
		init2PowersTable();
	}

	protected void initTransacWeightsArea() {
		transacWeights = new double[subjData.iCPerLine.length];
		if (designPoint.maxColumnsNb == -1) {
			transacWeights[0] = ((double) subjData.iCPerCell[0].length) * Math.pow(2, subjData.iCPerCell[0].length - 1);
			for (int i = 1; i < transacWeights.length; i++) {
				// transacWeights[i] = Math.pow(2, subjData.iCPerCell[i].length)-1;
				transacWeights[i] = ((double) subjData.iCPerCell[i].length)
						* Math.pow(2, subjData.iCPerCell[i].length - 1);
				transacWeights[i] += transacWeights[i - 1];
			}
			for (int i = 0; i < transacWeights.length; i++) {
				transacWeights[i] /= transacWeights[transacWeights.length - 1];
			}
		} else {
			transacWeights[0] = getSumOfComb(designPoint.maxColumnsNb, subjData.iCPerCell[0].length);
			for (int i = 1; i < transacWeights.length; i++) {
				// transacWeights[i] = Math.pow(2, subjData.iCPerCell[i].length)-1;
				transacWeights[i] = getSumOfComb(designPoint.maxColumnsNb, subjData.iCPerCell[i].length);
				transacWeights[i] += transacWeights[i - 1];
			}
			for (int i = 0; i < transacWeights.length; i++) {
				transacWeights[i] /= transacWeights[transacWeights.length - 1];
			}
		}
	}

	public double getSumOfComb(int L, int n) {
		long sum = 0;
		long comb = 1;
		int maxV = (n >= L) ? L : n;
		for (int k = 0; k <= maxV; k++) {
			sum += (comb * k);
			comb = comb * (n - k) / (k + 1);
		}
		return sum;
	}

	public double subWeightAreaF(int n, int m, int L) {
		int minV = n;
		long sum = 0;
		if (minV > L - m) {
			minV = L - m;
		}
		long comb = 1;
		for (int k = 0; k <= minV; k++) {
			sum += (comb * (k + m));
			comb = comb * (n - k) / (k + 1);
		}
		return sum;
	}

	private Pattern sampleInitPatternArea() {
		if (designPoint.maxColumnsNb == -1) {
			double r = Utilities.rGen.nextDouble();
			int indexTrans = Arrays.binarySearch(transacWeights, r);
			if (indexTrans < 0) {
				indexTrans += 1;
				indexTrans *= -1;
			}
			LinkedList<Integer> itemsetList = new LinkedList<>();
			double s = subjData.iCPerCell[indexTrans].length;
			for (int i = 0; i < subjData.iCPerCell[indexTrans].length; i++) {
				r = Utilities.rGen.nextDouble();
				if (2 * s * r <= (s + 1)) {
					// add it
					itemsetList.add(subjData.database.transactions[indexTrans][i]);
					s++;
				} else {
					// remove it
					s--;
				}
			}
			LinkedList<Integer> rows = supportComp.getRowsList(itemsetList);
			Pattern p = new Pattern(itemsetList, rows, 0, 0);
			supportComp.computeIC(p);
			return p;
		} else {
			double r = Utilities.rGen.nextDouble();
			int indexTrans = Arrays.binarySearch(transacWeights, r);
			if (indexTrans < 0) {
				indexTrans += 1;
				indexTrans *= -1;
			}
			LinkedList<Integer> itemsetList = new LinkedList<>();
			for (int i = 0; i < subjData.iCPerCell[indexTrans].length; i++) {
				double denominator = subWeightAreaF(subjData.iCPerCell[indexTrans].length - i, itemsetList.size(),
						designPoint.maxColumnsNb);
				double numerator = subWeightAreaF(subjData.iCPerCell[indexTrans].length - i - 1, itemsetList.size() + 1,
						designPoint.maxColumnsNb);
				double proba = numerator / denominator;
				r = Utilities.rGen.nextDouble();
				if (r <= proba) {
					itemsetList.add(subjData.database.transactions[indexTrans][i]);
				} else {
				}
			}
			LinkedList<Integer> rows = supportComp.getRowsList(itemsetList);
			Pattern p = new Pattern(itemsetList, rows, 0, 0);
			supportComp.computeIC(p);
			return p;
		}
	}
//	protected void initTransacWeightsFreq() {
//		transacWeights = new double[subjData.iCPerLine.length];
//		transacWeights[0] = Math.pow(2, subjData.iCPerCell[0].length) - 1;
//		// transacWeights[0]=((double)subjData.iCPerCell[0].length)*Math.pow(2,subjData.iCPerCell[0].length-1);
//		for (int i = 1; i < transacWeights.length; i++) {
//			transacWeights[i] = Math.pow(2, subjData.iCPerCell[i].length) - 1;
//			// transacWeights[i]=((double)subjData.iCPerCell[i].length)*Math.pow(2,subjData.iCPerCell[i].length-1);
//			transacWeights[i] += transacWeights[i - 1];
//		}
//		for (int i = 0; i < transacWeights.length; i++) {
//			transacWeights[i] /= transacWeights[transacWeights.length - 1];
//		}
//	}

	public void init2PowersTable() {
		int sizeTab = subjData.database.maxSizeOfTrans + 1;
		sizeTab = sizeTab >= subjData.iCPerCell.length ? sizeTab : subjData.iCPerCell.length + 1;
		twoPowersTab = new double[sizeTab];
		double d = 1;
		for (int i = 0; i < twoPowersTab.length; i++) {
			twoPowersTab[i] = d;
			d *= 2;
		}
	}

//	private Pattern sampleInitPatternFreq() {
//		double r = Utilities.rGen.nextDouble();
//		int indexTrans = Arrays.binarySearch(transacWeights, r);
//		if (indexTrans < 0) {
//			indexTrans += 1;
//			indexTrans *= -1;
//		}
//		LinkedList<Integer> itemsetList = new LinkedList<>();
//		double s = subjData.iCPerCell[indexTrans].length;
//		for (int i = 0; i < subjData.iCPerCell[indexTrans].length; i++) {
//			r = Utilities.rGen.nextDouble();
//			double bound = 0.5;
//			if (itemsetList.size() == 0) {
//				if (i == subjData.iCPerCell[indexTrans].length - 1) {
//					bound = 1;
//				} else {
//					bound = Math.pow(2, subjData.iCPerCell[indexTrans].length - i - 1);
//					bound = bound / (2 * bound - 1);
//				}
//			}
//			if (r <= bound) {
//				itemsetList.add(subjData.database.transactions[indexTrans][i]);
//			}
//		}
//		LinkedList<Integer> rows = supportComp.getRowsList(itemsetList);
//		Pattern p = new Pattern(itemsetList, rows, 0, 0);
//		supportComp.computeIC(p);
//		return p;
//	}

	private void sampleManyPattern(Pattern [] sampledPatterns,int curIdP) {
		Pattern p;
		// if (designPoint.initMeasure == Measure.area) {
		p = sampleInitPatternArea();
		for (int i = 0; i < designPoint.nbGibbsIterations; i++) {
			// we sample rows while columns are fixed
			//if (i > 0) {
				long startTime = System.currentTimeMillis();
				p.rows = supportComp.getRowsList(p.columns);
				long stopTime = System.currentTimeMillis();
				statistics.updateRowsTime += stopTime - startTime;

				startTime = System.currentTimeMillis();
				supportComp.computeIC(p);
				stopTime = System.currentTimeMillis();
				statistics.computeICTime += stopTime - startTime;
			//}
			 startTime = System.currentTimeMillis();
			sampleRowsAndUpdateP(p);
			 stopTime = System.currentTimeMillis();
			statistics.sampleRowsTime += stopTime - startTime;
			// we sample columns while rows are fixed

			startTime = System.currentTimeMillis();
			p.columns = supportComp.getColumnsList(p.rows);
			stopTime = System.currentTimeMillis();
			statistics.updateColumnsTime += stopTime - startTime;

			startTime = System.currentTimeMillis();
			supportComp.computeIC(p);
			stopTime = System.currentTimeMillis();
			statistics.computeICTime += stopTime - startTime;

			startTime = System.currentTimeMillis();
			if (designPoint.maxColumnsNb == -1) {
				sampleColumnsAndUpdateP(p);
			} else {
				sampleColumnsWithMaxColumnSize(p);
			}
			stopTime = System.currentTimeMillis();
			statistics.sampleColumnsTime += stopTime - startTime;
			startTime = System.currentTimeMillis();
			supportComp.computeIC(p);
			stopTime = System.currentTimeMillis();
			try {
				sampledPatterns[curIdP+i]=(Pattern) p.clone();
				sampledPatterns[curIdP+i].id=curIdP+i;
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private Pattern sampleOnePattern() {
		// we first generate a starting pattern
		Pattern p;
		// if (designPoint.initMeasure == Measure.area) {
		p = sampleInitPatternArea();
		// } else {
		// p = sampleInitPatternFreq();
		// }
		for (int i = 0; i < designPoint.nbGibbsIterations; i++) {
			// we sample rows while columns are fixed
			//if (i > 0) {
				long startTime = System.currentTimeMillis();
				p.rows = supportComp.getRowsList(p.columns);
				long stopTime = System.currentTimeMillis();
				statistics.updateRowsTime += stopTime - startTime;

				startTime = System.currentTimeMillis();
				supportComp.computeIC(p);
				stopTime = System.currentTimeMillis();
				statistics.computeICTime += stopTime - startTime;
			//}
			 startTime = System.currentTimeMillis();
			sampleRowsAndUpdateP(p);
			 stopTime = System.currentTimeMillis();
			statistics.sampleRowsTime += stopTime - startTime;
			// we sample columns while rows are fixed

			startTime = System.currentTimeMillis();
			p.columns = supportComp.getColumnsList(p.rows);
			stopTime = System.currentTimeMillis();
			statistics.updateColumnsTime += stopTime - startTime;

			startTime = System.currentTimeMillis();
			supportComp.computeIC(p);
			stopTime = System.currentTimeMillis();
			statistics.computeICTime += stopTime - startTime;

			startTime = System.currentTimeMillis();
			if (designPoint.maxColumnsNb == -1) {
				sampleColumnsAndUpdateP(p);
			} else {
				sampleColumnsWithMaxColumnSize(p);
			}
			stopTime = System.currentTimeMillis();
			statistics.sampleColumnsTime += stopTime - startTime;
		}
		supportComp.computeIC(p);
		return p;
	}

	private void sampleColumnsAndUpdateP(Pattern p) {
		// don't forget to compute ic at the end
		LinkedList<Integer> newColumnsList = new LinkedList<>();
		double sizeR1 = 0;
		double sizeR2 = p.columns.size();
		double icR1 = 0;
		double icR2 = p.ic;
		double beta = (double) (p.rows.size())
				+ ((double) (subjData.database.columnsNames.length + subjData.database.linesNames.length)
						* subjData.gamma / subjData.alpha);
		double fR2Beta = fComon((int) sizeR2, beta);
		// double twoPowerSizeR2= Math.pow(2, sizeR2);
		double twoPowerSizeR2 = twoPowersTab[(int) sizeR2];
		// double fR2_1BetaPlus1 = fComon((int) sizeR2 - 1, beta + 1);
		int cpt = 0;
		for (int iId : p.columns) {
			double proba;
			double icI = p.itemsetIC[cpt];
			if (sizeR2 > 1) {
				// important : don't forget to make specific case of sizeR2=1
				// double fR2_1Beta = fR2Beta * (1 + beta / sizeR2) - Math.pow(2, sizeR2) /
				// sizeR2;
				double fR2_1BetaPlus1 = twoPowerSizeR2 / sizeR2 - fR2Beta * beta / sizeR2;
				double fR2_2BetaPlus2 = twoPowerSizeR2 / (2 * (sizeR2 - 1.))
						- fR2_1BetaPlus1 * (beta + 1.) / (sizeR2 - 1.);
				double numerator = (icR1 + icI) * fR2_1BetaPlus1 + (icR2 - icI) * fR2_2BetaPlus2;
				double denominator = icR1 * fR2Beta + icR2 * fR2_1BetaPlus1;
				proba = numerator / denominator;
			} else {
				double numerator = (icR1 + icI) / (beta + 1.);
				double denominator = icR1 / beta + numerator;
				proba = numerator / denominator;
			}
			double r = Utilities.rGen.nextDouble();
			if (r < proba) {
				// we add rId
				newColumnsList.add(iId);
				icR1 += icI;
				icR2 -= icI;
				fR2Beta = (twoPowerSizeR2 - beta * fR2Beta) / ((double) sizeR2);
				// fR2_1BetaPlus1 = (Math.pow(2, sizeR2 - 1) - (beta + 1) * fR2_1BetaPlus1) /
				// ((double) (sizeR2 - 1));
				beta++;
				sizeR2--;
				twoPowerSizeR2 /= 2;
				sizeR1++;
			} else {
				icR2 -= icI;
				fR2Beta = (1 + beta / sizeR2) * fR2Beta - twoPowerSizeR2 / sizeR2;
				// fR2_1BetaPlus1 = (1 + (beta + 1.) / (sizeR2 - 1.)) * fR2_1BetaPlus1
				// - Math.pow(2, sizeR2 - 1.) / (sizeR2 - 1.);
				sizeR2--;
				twoPowerSizeR2 /= 2;
			}
			cpt++;
		}
		// update P: change rowsList, itemsIC, rowsIC, and IC
		// deplacer cette partie avec mise a jour de rowsList
		p.columns = newColumnsList;
		// supportComp.computeIC(p);

	}

	private double getWeightSIWithColumnSize(double beta, double sizeR1, double sizeR2, int L, double icR1,
			double icR2) {
		double comb1 = 1;
		double comb2;
		double summ = 0;
		int maxV = (int) (L - sizeR1);
		if (maxV > sizeR2)
			maxV = (int) sizeR2;
		for (int k = 0; k <= maxV; k++) {
			comb2 = comb1 * k / sizeR2;
			summ += ((comb1 * icR1 + comb2 * icR2) / ((double) k + beta));
			comb1 = comb1 * (sizeR2 - k) / (k + 1);
		}
		return summ;
	}

	private void sampleColumnsWithMaxColumnSize(Pattern p) {
		// don't forget to compute ic at the end
		LinkedList<Integer> newColumnsList = new LinkedList<>();
		double sizeR1 = 0;
		double sizeR2 = p.columns.size();
		double icR1 = 0;
		double icR2 = p.ic;
		double beta = (double) (p.rows.size())
				+ ((double) (subjData.database.columnsNames.length + subjData.database.linesNames.length)
						* subjData.gamma / subjData.alpha);
		int cpt = 0;
		for (int iId : p.columns) {
			if (newColumnsList.size() >= designPoint.maxColumnsNb) {
				break;
			}
			double proba;
			double icI = p.itemsetIC[cpt];
			double denominator = getWeightSIWithColumnSize(beta, sizeR1, sizeR2, designPoint.maxColumnsNb, icR1, icR2);
			double numerator = getWeightSIWithColumnSize(beta + 1, sizeR1 + 1, sizeR2 - 1, designPoint.maxColumnsNb,
					icR1 + icI, icR2 - icI);
			proba = numerator / denominator;
			if (proba>1) {
				System.out.println("problem");
			}
			double r = Utilities.rGen.nextDouble();
			if (r < proba) {
				// we add rId
				newColumnsList.add(iId);
				icR1 += icI;
				icR2 -= icI;
				beta++;
				sizeR2--;
				sizeR1++;
			} else {
				icR2 -= icI;
				// fR2_1BetaPlus1 = (1 + (beta + 1.) / (sizeR2 - 1.)) * fR2_1BetaPlus1
				// - Math.pow(2, sizeR2 - 1.) / (sizeR2 - 1.);
				sizeR2--;
			}
			cpt++;
		}
		// update P: change rowsList, itemsIC, rowsIC, and IC
		// deplacer cette partie avec mise a jour de rowsList
		p.columns = newColumnsList;
		// supportComp.computeIC(p);

	}

	private void sampleRowsAndUpdateP(Pattern p) {
		// don't forget to compute ic at the end
		LinkedList<Integer> newRowsList = new LinkedList<>();
		double sizeR1 = 0;
		double sizeR2 = p.rows.size();
		double icR1 = 0;
		double icR2 = p.ic;
		double beta = (double) (p.columns.size())
				+ ((double) (subjData.database.columnsNames.length + subjData.database.linesNames.length))
						* subjData.gamma / subjData.alpha;
		double fR2Beta = fComon((int) sizeR2, beta);
		// double fR2_1BetaPlus1 = fComon((int) sizeR2 - 1, beta + 1);
		int cpt = 0;
		// double twoPowerSizeR2= Math.pow(2, sizeR2);
		double twoPowerSizeR2 = twoPowersTab[(int) sizeR2];
		double fR2_1BetaPlus1;
		double fR2_2BetaPlus2;
		double numerator;
		double denominator;
		double r;
		for (int rId : p.rows) {
			double proba;
			double icI = p.rowsIC[cpt];
			if (sizeR2 > 1) {
				// important : don't forget to make specific case of sizeR2=1
				// double fR2_1Beta = fR2Beta * (1 + beta / sizeR2) - Math.pow(2, sizeR2) /
				// sizeR2;
				fR2_1BetaPlus1 = twoPowerSizeR2 / sizeR2 - fR2Beta * beta / sizeR2;
				fR2_2BetaPlus2 = twoPowerSizeR2 / (2 * (sizeR2 - 1.)) - fR2_1BetaPlus1 * (beta + 1.) / (sizeR2 - 1.);
				numerator = (icR1 + icI) * fR2_1BetaPlus1 + (icR2 - icI) * fR2_2BetaPlus2;
				denominator = icR1 * fR2Beta + icR2 * fR2_1BetaPlus1;
				proba = numerator / denominator;
			} else {
				numerator = (icR1 + icI) / (beta + 1.);
				denominator = icR1 / beta + numerator;
				proba = numerator / denominator;
			}
			r = Utilities.rGen.nextDouble();
			if (r < proba) {
				// we add rId
				newRowsList.add(rId);
				icR1 += icI;
				icR2 -= icI;
				fR2Beta = (twoPowerSizeR2 - beta * fR2Beta) / ((double) sizeR2);
				// fR2_1BetaPlus1 = (Math.pow(2, sizeR2 - 1) - (beta + 1) * fR2_1BetaPlus1) /
				// ((double) (sizeR2 - 1));
				beta++;
				sizeR2--;
				twoPowerSizeR2 /= 2;
				sizeR1++;
			} else {
				icR2 -= icI;
				fR2Beta = (1 + beta / sizeR2) * fR2Beta - twoPowerSizeR2 / sizeR2;
				// fR2_1BetaPlus1 = (1 + (beta + 1.) / (sizeR2 - 1.)) * fR2_1BetaPlus1
				// - Math.pow(2, sizeR2 - 1.) / (sizeR2 - 1.);
				sizeR2--;
				twoPowerSizeR2 /= 2;
			}
			cpt++;
		}

		// update P: change rowsList, itemsIC, rowsIC, and IC
		p.rows = newRowsList;
		// deplacer cette partie avec mise a jour de columnsList
		// supportComp.computeIC(p);
	}

	public double fComon(int n, double beta) {
		double sum = 0;
		double comb = 1;
		for (int k = 0; k <= n; k++) {
			sum += comb / ((double) (k) + beta);
			comb = comb * ((double) (n - k)) / ((double) (k + 1));
		}
		return sum;
	}

	public Pattern[] samplePatterns() {
		if (transacWeights == null) {
			long startTime = System.currentTimeMillis();
			// if (designPoint.initMeasure == Measure.area) {
			initTransacWeightsArea();
			// } else {
			// initTransacWeightsFreq();
			// }
			long stopTime = System.currentTimeMillis();
			statistics.initWeightsTime = stopTime - startTime;
		}
		long startTime = System.currentTimeMillis();
		sampledPatterns = new Pattern[designPoint.nbSampledPatterns];
		for (int i = 0; i < designPoint.nbSampledPatterns; i++) {
			if (i%1000==0) {
				System.out.println("i+1:"+String.valueOf(i+1));
			}
			sampledPatterns[i] = sampleOnePattern();
//			if (sampledPatterns[i].rows.size()>1) {
//				System.out.println("generated more than one row:"+sampledPatterns[i].rows.size()+",nb col:"+sampledPatterns[i].itemset.size());
//			}
			sampledPatterns[i].id = i;
		}
		long stopTime = System.currentTimeMillis();
		statistics.samplingTime = stopTime - startTime;
		return sampledPatterns;
	}
	
	public Pattern[] samplePatternsAndKeepAll() {
		if (transacWeights == null) {
			long startTime = System.currentTimeMillis();
			// if (designPoint.initMeasure == Measure.area) {
			initTransacWeightsArea();
			// } else {
			// initTransacWeightsFreq();
			// }
			long stopTime = System.currentTimeMillis();
			statistics.initWeightsTime = stopTime - startTime;
		}
		long startTime = System.currentTimeMillis();
		sampledPatterns = new Pattern[designPoint.nbSampledPatterns*designPoint.nbGibbsIterations];
		for (int i = 0; i < designPoint.nbSampledPatterns; i++) {
			if (i%10==0) {
				System.out.println("i+1:"+String.valueOf(i+1));
			}
			//sampledPatterns[i] = sampleOnePattern();
//			if (sampledPatterns[i].rows.size()>1) {
//				System.out.println("generated more than one row:"+sampledPatterns[i].rows.size()+",nb col:"+sampledPatterns[i].itemset.size());
//			}
			sampleManyPattern(sampledPatterns, i*designPoint.nbGibbsIterations);
			//sampledPatterns[i].id = i;
		}
		long stopTime = System.currentTimeMillis();
		statistics.samplingTime = stopTime - startTime;
		return sampledPatterns;
	}

	public void computeNonRedundantPatternsWithHash() {
		long startTime = System.currentTimeMillis();
		nonRedundantMap = new HashMap<>();
		ArrayList<Pattern> nonRedundant = new ArrayList<>();
		for (Pattern p : sampledPatterns) {
			if (nonRedundantMap.containsKey(p)) {
				nonRedundantMap.get(p).nbGeneratedTimes += 1;
			} else {
				nonRedundantMap.put(p, p);
				p.nbGeneratedTimes = 1;
				nonRedundant.add(p);
			}
		}
		nonRedundantPatterns = new Pattern[nonRedundant.size()];
		for (int i = 0; i < nonRedundant.size(); i++) {
			nonRedundantPatterns[i] = nonRedundant.get(i);
		}
		long stopTime = System.currentTimeMillis();
		statistics.removeRedundancyTime = stopTime - startTime;
	}
}
