package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.Database;
import model.Pattern;
import model.SubjectiveData;
import model.WritableResults;
import utils.DesignPoint;
import utils.InputFormat;
import utils.Statistics;
import utils.Utilities;

public class InputOutputManager {

	public static Database loadDatabase(DesignPoint designPoint) {
		if (designPoint.inputFormat == InputFormat.complete) {
			return loadCompleteFormat(designPoint);
		} else {
			return loadSparseFormat(designPoint);
		}
	}

	private static Database loadCompleteFormat(DesignPoint designPoint) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(designPoint.inputFilePath));
			String line = reader.readLine();
			line = line.replace("\n", "").replace("\r", "");
			String[] elements = line.split(" ");
			String[] columnNames = Arrays.copyOfRange(elements, 1, elements.length);
			ArrayList<int[]> transactionsList = new ArrayList<>();
			ArrayList<String> linesNames = new ArrayList<>();
			int maxSizeTrans=0;
			while ((line = reader.readLine()) != null) {
				line = line.replace("\n", "").replace("\r", "");
				elements = line.split(" ");
				linesNames.add(elements[0]);
				int nbItems = 0;
				for (int i = 1; i < elements.length; i++) {
					if (elements[i].equals("1")) {
						nbItems++;
					}
				}

				int[] itemset = new int[nbItems];
				int cpt = 0;
				for (int i = 1; i < elements.length; i++) {
					if (elements[i].equals("1")) {
						itemset[cpt] = i - 1;
						cpt++;
					}

				}
				transactionsList.add(itemset);
				maxSizeTrans=maxSizeTrans>=itemset.length?maxSizeTrans:itemset.length;
			}
			reader.close();
			int[][] transactions = new int[transactionsList.size()][];
			String[] linesNamesArray = new String[linesNames.size()];
			for (int i = 0; i < transactions.length; i++) {
				transactions[i] = transactionsList.get(i);
				linesNamesArray[i] = linesNames.get(i);
			}
			Database db = new Database(transactions, columnNames, linesNamesArray,maxSizeTrans);
			db.updateTransactionsMaps();
			return db;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Database loadSparseFormat(DesignPoint designPoint) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(designPoint.inputFilePath));
			String[] columnNames = null;
			ArrayList<String> linesNames = new ArrayList<>();
			ArrayList<int[]> transactionsList = new ArrayList<>();
			String line;
			int cpt = 0;
			int maxValue = 0;
			int maxSizeTrans=0;
			while ((line = reader.readLine()) != null) {
				line = line.replace("\n", "").replace("\r", "");
				String[] elements = line.split(" ");
				linesNames.add("line" + String.valueOf(cpt));
				int[] itemset = new int[elements.length];
				for (int i = 0; i < elements.length; i++) {
					itemset[i] = Integer.parseInt(elements[i]);
					maxValue = (maxValue >= itemset[i]) ? maxValue : itemset[i];
				}
				maxSizeTrans=maxSizeTrans>=itemset.length?maxSizeTrans:itemset.length;
				transactionsList.add(itemset);

				cpt++;
			}
			columnNames = new String[maxValue + 1];
			for (int i = 0; i < columnNames.length; i++) {
				columnNames[i] = "att" + String.valueOf(i);
			}
			reader.close();
			int[][] transactions = new int[transactionsList.size()][];
			String[] linesNamesArray = new String[linesNames.size()];
			for (int i = 0; i < transactions.length; i++) {
				transactions[i] = transactionsList.get(i);
				linesNamesArray[i] = linesNames.get(i);
			}
			Database db = new Database(transactions, columnNames, linesNamesArray,maxSizeTrans);
			db.updateTransactionsMaps();
			Pattern.nbTotalColumnsInD = db.columnsNames.length;
			return db;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static SubjectiveData computeMaxEntResultsFromDatabase(Database database, DesignPoint designPoint,
			Statistics statistics) {
		try {

			BufferedWriter writer = new BufferedWriter(
					new FileWriter(designPoint.outputFolderPath + "/" + DesignPoint.maxEntInputFileName));
			for (int i = 0; i < database.transactions.length; i++) {
				writer.write(String.valueOf(database.transactions[i].length));
				for (int j : database.transactions[i]) {
					writer.write(" " + String.valueOf(j));
				}
				writer.write("\n");
			}
			writer.close();
			long startTime = System.currentTimeMillis();
			Process p = Runtime.getRuntime()
					.exec("./MaxEntComputer " + designPoint.outputFolderPath + "/" + DesignPoint.maxEntInputFileName
							+ " " + designPoint.outputFolderPath + "/" + DesignPoint.maxEntLambdasFileName);// + " "
							//+ designPoint.outputFolderPath + "/" + DesignPoint.maxEntProbasFileName);
			p.waitFor();
			long stopTime = System.currentTimeMillis();
			statistics.maxEntTime = stopTime - startTime;
			double[] lambdaLines = new double[database.linesNames.length];
			double[] lambdaColumns = new double[database.columnsNames.length];
			BufferedReader reader = new BufferedReader(
					new FileReader(designPoint.outputFolderPath + "/" + DesignPoint.maxEntLambdasFileName));
			String line = reader.readLine();
			int cpt = 0;
			while ((line = reader.readLine()) != null) {
				if (line.contains("lambdaColumns")) {
					break;
				}
				lambdaLines[cpt] = Double.parseDouble(line.replace("\n", "").replace("\r", ""));
				cpt++;
			}
			assert (cpt == database.linesNames.length);
			cpt = 0;
			while ((line = reader.readLine()) != null) {
				lambdaColumns[cpt] = Double.parseDouble(line.replace("\n", "").replace("\r", ""));
				cpt++;
			}
			assert (cpt == database.columnsNames.length);
			reader.close();
			double[][] iCPerCell = new double[database.transactions.length][];
			double[] iCPerLine = new double[database.transactions.length];
			for (int i = 0; i < database.transactions.length; i++) {
				iCPerLine[i] = 0;
				iCPerCell[i] = new double[database.transactions[i].length];
				cpt = 0;
				for (int j : database.transactions[i]) {
					double expo = Math.exp(lambdaLines[i] + lambdaColumns[j]);
					double pij = expo / (expo + 1);
					iCPerCell[i][cpt] = -Utilities.log2(pij);
					iCPerLine[i] += iCPerCell[i][cpt];
					cpt++;
				}
			}

			return new SubjectiveData(database, lambdaLines, lambdaColumns, iCPerCell, iCPerLine);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static void writeResults(PatternSampler sampler) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try (FileWriter writer = new FileWriter(sampler.designPoint.outputFolderPath + "/statistics.json")) {
			gson.toJson(sampler.statistics, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (FileWriter writer = new FileWriter(sampler.designPoint.outputFolderPath + "/designPoint.json")) {
			gson.toJson(sampler.designPoint, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		WritableResults res = new WritableResults();
		res.nbPatterns = sampler.sampledPatterns.length;
		res.patterns = sampler.sampledPatterns;

		if (sampler.designPoint.writeAllResults) {
			try (FileWriter writer = new FileWriter(sampler.designPoint.outputFolderPath + "/results.json")) {
				gson.toJson(res, writer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (sampler.designPoint.writeNonRedundantResults) {
			WritableResults res2 = new WritableResults();
			res2.nbPatterns = sampler.nonRedundantPatterns.length;
			res2.patterns = sampler.nonRedundantPatterns;
			try (FileWriter writer = new FileWriter(
					sampler.designPoint.outputFolderPath + "/nonredundantresults.json")) {
				gson.toJson(res2, writer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
