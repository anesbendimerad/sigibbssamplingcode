package controller;

import java.util.LinkedList;

import org.apache.lucene.util.OpenBitSet;

import model.BitSetIterator;
import model.Integer2;
import model.Pattern;
import model.SubjectiveData;

public class BitsetSupportComputer {
	private SubjectiveData subjData;
	
	private OpenBitSet[] horizontalBitsets;
	public OpenBitSet hInterm;
	
	private OpenBitSet[] verticalBitsets;
	public OpenBitSet vInterm;
	
	public boolean init=false;
	public BitsetSupportComputer(SubjectiveData subjData) {
		this.subjData = subjData;
	}
	
	public void initBitSets(){
		if (!init) {
			init=true;
			verticalBitsets=new OpenBitSet[subjData.database.columnsNames.length];
			for (int i=0;i<subjData.database.columnsNames.length;i++){
				verticalBitsets[i]=new OpenBitSet(subjData.database.linesNames.length);
			}
			horizontalBitsets=new OpenBitSet[subjData.database.linesNames.length];
			for (int i=0;i<subjData.database.linesNames.length;i++){
				horizontalBitsets[i]=new OpenBitSet(subjData.database.columnsNames.length);
				
			}
			for (int i=0;i<subjData.database.linesNames.length;i++){
				for (int item : subjData.database.transactions[i]){
					verticalBitsets[item].fastSet(i);
					horizontalBitsets[i].fastSet(item);
				}
			}
			vInterm=new OpenBitSet(subjData.database.linesNames.length);
			hInterm=new OpenBitSet(subjData.database.columnsNames.length);
			
		}
	}
	
	public int getSupportOfPattern(Pattern p){
		vInterm.set(0,subjData.database.linesNames.length);
		for (int i : p.columns){
			vInterm.and(verticalBitsets[i]);
		}
		return (int)vInterm.cardinality();
	}
	public void updateWithPSupport(Pattern p){
		vInterm.set(0,subjData.database.linesNames.length);
		for (int i : p.columns){
			vInterm.and(verticalBitsets[i]);
		}
	}
	public LinkedList<Integer> getRowsList(LinkedList<Integer> items){
		vInterm.set(0,subjData.database.linesNames.length);
		for (int i : items){
			vInterm.and(verticalBitsets[i]);
		}
		BitSetIterator iterator=new BitSetIterator(vInterm);
		LinkedList<Integer> lst=new LinkedList<>();
		int curIt;
		while ((curIt=iterator.getNext())>=0) {
			lst.add(curIt);
		}
		return lst;
	}
	
	public LinkedList<Integer> getColumnsList(LinkedList<Integer> rows){
		hInterm.set(0,subjData.database.columnsNames.length);
		for (int i : rows){
			hInterm.and(horizontalBitsets[i]);
		}
		BitSetIterator iterator=new BitSetIterator(hInterm);
		LinkedList<Integer> lst=new LinkedList<>();
		int curIt;
		while ((curIt=iterator.getNext())>=0) {
			lst.add(curIt);
		}
		return lst;
	}
	public void computeIC(Pattern p) {
		p.ic=0;
		p.itemsetIC=new double[p.columns.size()];
		p.rowsIC=new double[p.rows.size()];
		//boolean initRowsIC=false;
		int curItemsetId=0;
		Integer2 key=new Integer2(0);
		for (int i : p.columns) {
			double curIIC=0;
			int curId=0;
			for (int j: p.rows) {
//				if (!initRowsIC) {
//					p.rowsIC.add(0.);
//				}
				key.value=i;
				double v=subjData.iCPerCell[j][subjData.database.transactionsMaps[j].get(key)];
				p.ic+=v;
				curIIC+=v;
				p.rowsIC[curId]+=v;
				//p.rowsIC.add(curId, p.rowsIC.remove(curId)+v);
				curId++;
			}
			//initRowsIC=true;
			p.itemsetIC[curItemsetId]+=curIIC;
			curItemsetId++;
		}
		p.si=p.ic/(subjData.alpha*((double)(p.columns.size()+p.rows.size())) + subjData.gamma*((double)(subjData.database.columnsNames.length+subjData.database.linesNames.length)));
	}
//	public void computeAllMeasures(Pattern[] patterns){
//		for (Pattern p : patterns){
//			updateWithPSupport(p);
//			p.support=(int)interm.cardinality();
//			BitSetIterator iterator=new BitSetIterator(interm);
//			int curIt=0;
//			double ic=0;
//			while ((curIt=iterator.getNext())>=0){
//				for (int item : p.itemset){
//					ic+=subjData.iCPerCell[curIt][subjData.database.transactionsMaps[curIt].get(item)];
//				}
//			}
//			p.ic=ic;
//			p.si=ic/(subjData.logE*((double)(1+p.itemset.size())));
//		}
//	}
	public void computePatternMeasure(Pattern p){
//		updateWithPSupport(p);
//		p.support=(int)interm.cardinality();
//		BitSetIterator iterator=new BitSetIterator(interm);
//		int curIt=0;
//		double ic=0;
//		while ((curIt=iterator.getNext())>=0){
//			for (int item : p.itemset){
//				ic+=subjData.iCPerCell[curIt][subjData.database.transactionsMaps[curIt].get(item)];
//			}
//		}
//		p.ic=ic;
//		p.si=ic/(subjData.logE*((double)(1+p.itemset.length)));
	}
}





