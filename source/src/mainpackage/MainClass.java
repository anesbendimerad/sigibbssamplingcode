package mainpackage;

import controller.BitsetSupportComputer;
import controller.InputOutputManager;
import controller.PatternSampler;
import model.Database;
import model.SubjectiveData;
import utils.DesignPoint;
import utils.Statistics;
import utils.Utilities;

public class MainClass {

	public static void main(String[] args) {
		DesignPoint designPoint = new DesignPoint();
		if (args.length != 1) {
			System.out.println(
					"you can specify one parameter : parameters File path. Otherwise, default values will be considered");
		} else {
			Utilities.readParametersFromFile(designPoint, args[0]);
		}
		Statistics statistics = new Statistics();
		Utilities.createFolder(designPoint.outputFolderPath, false);
		Database database = InputOutputManager.loadDatabase(designPoint);
		SubjectiveData subjectiveData = InputOutputManager.computeMaxEntResultsFromDatabase(database, designPoint,
				statistics);
		BitsetSupportComputer sComp = new BitsetSupportComputer(subjectiveData);
		PatternSampler sampler = new PatternSampler(designPoint, statistics, subjectiveData,sComp);

		if (designPoint.keepAll) {
			sampler.samplePatternsAndKeepAll();
		}
		else
		{
			sampler.samplePatterns();
		}
		

		// calculate support if DesignPoint.computeSupport (we use
		// openbitsets here)
		//if (designPoint.computeMeasures) {
			
			//sComp.initBitSets();
			//sComp.computeAllMeasures(sampler.sampledPatterns);
		//}
		if (designPoint.writeNonRedundantResults) {
			sampler.computeNonRedundantPatternsWithHash();
		}
		// write results
		InputOutputManager.writeResults(sampler);
	}

}
