package utils;

public class Statistics {
	public long initWeightsTime;
	public long samplingTime;
	public long maxEntTime;
	public long computeSupportTime; // post processing time
	public long removeRedundancyTime;
	
	public long sampleColumnsTime;
	public long sampleRowsTime;
	public long updateRowsTime;
	public long updateColumnsTime;
	
	public long computeICTime;
	
}
