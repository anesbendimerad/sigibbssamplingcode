
package utils;

import model.Measure;

public class DesignPoint {
	public final static String maxEntInputFileName="inputMaxEnt.txt";
	public final static String maxEntLambdasFileName="lambdasMaxEnt.txt";
	public final static String maxEntProbasFileName="probasMaxEnt.txt";
	public String inputFilePath;
	public String outputFolderPath;
	//public boolean computeMeasures=true;
	//public QualityMeasure qualityMeasure=QualityMeasure.IC;
	public int nbSampledPatterns=1;
	public int nbGibbsIterations=100;
	public InputFormat inputFormat=InputFormat.sparse;
	public boolean writeNonRedundantResults=true;
	public boolean writeAllResults=false;
	public Measure initMeasure=Measure.area;
	//public int maxRowsNb=-1;
	public int maxColumnsNb=-1;
	public boolean keepAll=false;
	//public boolean applyUpdating=false;
	//public int nbStepBeforeUpdating=100;
	//public QualityMeasure maximizedMeasureInUpdate=QualityMeasure.IC;
}
