# Gibbs sampling subjectively interesting tiles

This repository provides source code and data for the following paper: 

Anes Bendimerad, Jefrey Lijffijt, Marc Plantevit, Céline Robardet, and Tijl De Bie. Gibbs sampling subjectively interesting tiles.

## Data:
The "Data" folder contains the input files required for running Gibbs-SI on the studied datasets: mushrooms, chess, kdd.

## source code:
In "source", we share the source code and the runnable JAR of the Gibbs-SI. This algorith requires a parameter file, an example of this file is given in "sourceCode/MICAMiner/parameters.txt":

```

inputFilePath=ItemsetDatasets/retail.txt

outputFolderPath=retailKeepAll

nbSampledPatterns=1000

nbGibbsIterations=100

```

To run this algorithm, the following command line can be executed from the "source" folder:


```

java -jar GibbsSI.jar parameters.txt

```
